//! This crate is a Rust library providing a plugin for [Rocket] loading and
//! managing configuration files for [Rocket].
//!
//! This library allows two configuration file formats: [YAML] and [JSON].
//! Deserialization is done using [serde] and specialized packages [serde_json]
//! and [serde_yaml].
//!
//! [JSON]: http://json.org
//! [Rocket]: https://rocket.rs/
//! [serde]: https://serde.rs/
//! [serde_json]: https://docs.serde.rs/serde_json/
//! [serde_yaml]: https://docs.serde.rs/serde_yaml/
//! [YAML]: http://yaml.org

#![feature(arbitrary_self_types, decl_macro, proc_macro_hygiene)]
#[macro_use] extern crate lazy_static;
extern crate rocket;
extern crate serde;

#[cfg(test)] #[macro_use] extern crate serde_json;
#[cfg(test)] extern crate tempfile;

pub mod configuration;
mod constants;
pub mod error;
pub mod factory;
mod result;
pub mod value;

pub use value::Value;
pub use factory::Factory;
pub use result::Result;